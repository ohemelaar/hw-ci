//! Getting the string required to display "Hello, World!".
//!
//! Provides the `get_message` method that acts as an abstractions for this purpose.

/// Returns the texts that's expected to be shown by a typical "Hello World!" program.
pub fn get_message() -> &'static str {
    "Hello, World!"
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_message_returns_hello_world() {
        assert_eq!(get_message(), "Hello, World!");
    }

    #[test]
    fn get_message_doesnt_return_just_works() {
        assert_ne!(get_message(), "It just works™");
    }
}
